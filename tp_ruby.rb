   require 'state_machine'
   require 'highline/import'

   class PopVehicle
      state_machine :initial => :parked do

         event :park do
            transition [:idling] => :parked
         end
         
         event :ignite do
            transition [:parked] => :idling
         end
         
         event :gear_up do
            transition [:idling] => :first_gear,
                       [:first_gear] => :second_gear,
                       [:second_gear] => :third_gear,
                       [:third_gear] => :crashed
         end
         
         event :gear_down do
            transition [:third_gear] => :second_gear,
                       [:second_gear] => :first_gear,
                       [:first_gear] => :idling
         end
         
         event :crash do
            transition [:first_gear, :second_gear, :third_gear] => :crashed
         end
         
         event :repair do
            transition [:crashed] => :parked
         end
         
      end
   end
   
   vehicle = PopVehicle.new
   
   
   while true do
      puts vehicle.state
      puts "État actuel : #{vehicle.state}"
      puts "Je peux : #{vehicle.state_events}"
      input = ask "Action ? : "
      vehicle.send(input)
   end
   
   
   
